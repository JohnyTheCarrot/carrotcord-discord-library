﻿using Carrotcord_API.Carrotcord.Stuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.API
{
    public class Invite
    {
        public int max_age { get; internal set; }
        public string code { get; internal set; }
        public Guild guild { get; internal set; }
        public string created_at { get; internal set; }
        public bool temporary { get; internal set; }
        public int uses { get; internal set; }
        public int max_uses { get; internal set; }
        public User inviter;
        public Channel channel;

        internal static Invite fromData(dynamic data)
        {
            return new Invite()
            {
                max_age = data.max_age,
                code = data.code,
                guild = Guild.fromJSON(data.guild),
                created_at = data.created_at,
                temporary = Convert.ToBoolean(data.temporary),
                uses = Convert.ToInt32(data.uses),
                max_uses = Convert.ToInt32(data.maxuses),
                inviter = User.FromData(data.inviter),
                channel = Channel.getChannel(Convert.ToInt64(data.channel.id))
            };
        }

        internal void register()
        {
            if (Storage.cachedInvites.TryGetValue(code, out Invite value))
            {
                return;
            }
            Storage.cachedInvites.Add(code, this);
        }

        public void delete()
        {
            JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.DELET_THIS($"guilds/{guild.ID}/invites/{code}"));
        }
    }
}
