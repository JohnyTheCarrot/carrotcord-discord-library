﻿using Carrotcord_API.Carrotcord.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.HTTP
{
    public class RatelimitTriggeredException : Exception
    {
        public RatelimitTriggeredException()
        {
            CarrotcordLogger.LogBork("(Too Many Requests) Ratelimit Triggered! Be a good boy now and don't do so much things at once.");
        }

        public RatelimitTriggeredException(string message) : base(message)
        {
            CarrotcordLogger.LogBork(message);
        }
    }
}
