﻿using Carrotcord_API.Carrotcord.API;
using Carrotcord_API.Carrotcord.Stuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.ERROR
{
    public class UnknownEmojiException : Exception
    {


        public UnknownEmojiException()
        {
            CarrotcordLogger.LogBork("Unknown Emoji");
        }

        public UnknownEmojiException(string message) : base(message)
        {
            CarrotcordLogger.LogBork(message);
        }
    }
}
