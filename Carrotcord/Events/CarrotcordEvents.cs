﻿using Carrotcord_API.Carrotcord.Stuff;
using Carrotcord_API.Carrotcord.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Events
{
    public delegate void ClientReadyEventHandler(object sender, ClientReadyEventArgs e);

    public class ClientReadyEventArgs : EventArgs
    {
        public SelfUser BotUser;

        public ClientReadyEventArgs(SelfUser BotUser)
        {
            this.BotUser = BotUser;
        }
    }

    public delegate void CarrotcordLoggerLogEvent(object sender, CarrotcordLoggerLogEventArgs e);

    public class CarrotcordLoggerLogEventArgs : EventArgs
    {
        public string logmessage;

        public CarrotcordLoggerLogEventArgs(string logmessage)
        {
            this.logmessage = logmessage;
        }
    }

    public delegate void MessageCreatedEventHandler(object sender, MessageCreatedEventArgs e);

    public class MessageCreatedEventArgs : EventArgs
    {
        public Message message;

        public MessageCreatedEventArgs(Message message)
        {
            this.message = message;
        }
    }

    public delegate void MessageDeletedEventHandler(object sender, MessageDeletedEventArgs e);

    /// <summary>
    /// Event args for the Message Deleted event
    /// </summary>
    public class MessageDeletedEventArgs : EventArgs
    {
        /// <summary>
        /// When the message is cached, (all messages received after bootup will be cached) this value will not be null.
        /// </summary>
        public Message message;
        /// <summary>
        /// Message ID, this value will never be null.
        /// </summary>
        public long message_id;
        /// <summary>
        /// Channel ID, this value will never be null.
        /// </summary>
        public long channel_id;
        /// <summary>
        /// Guild ID, this value will never be null.
        /// </summary>
        public long guild_id;
        public bool cached { get; internal set; }

        public MessageDeletedEventArgs(Message message)
        {
            this.message = message;
            message_id = message.ID;
            channel_id = message.channelID;
            guild_id = message.guildID;
            cached = true;
        }

        public MessageDeletedEventArgs(long ID, long channel, long guild)
        {
            message_id = ID;
            channel_id = channel;
            guild_id = guild;
            cached = false;
        }

    }

    public delegate void ChannelCreatedEventHandler(object sender, ChannelCreatedEventArgs e);

    public class ChannelCreatedEventArgs : EventArgs
    {
        public Channel channel { get; internal set; }

        internal ChannelCreatedEventArgs(Channel c)
        {
            channel = c;
        }
    }

    public delegate void ChannelDeletedEventHandler(object sender, ChannelDeletedEventArgs e);

    public class ChannelDeletedEventArgs : EventArgs
    {
        public Channel channel { get; internal set; }

        internal ChannelDeletedEventArgs(Channel c)
        {
            channel = c;
        }
    }

    public delegate void CommandExecutedEventHandler(object sender, CommandExecutedEventArgs e);

    public class CommandExecutedEventArgs : EventArgs
    {
        public string CommandName { get; set; }
        public User user { get; internal set; }

        internal CommandExecutedEventArgs(Command c)
        {
            CommandName = c.name;
            
        }
    }

    public delegate void GuildMemberUpdateEventHandler(object sender, GuildMemberUpdateEventArgs e);

    public class GuildMemberUpdateEventArgs : EventArgs
    {
        public User user { get; internal set; }

        internal GuildMemberUpdateEventArgs(User u)
        {
            user = u;
        }
    }

    public delegate void GuildMemberRemoveEventHandler(object sender, GuildMemberRemoveEventArgs e);

    public class GuildMemberRemoveEventArgs : EventArgs
    {
        public User user { get; internal set; }
        public Guild guild { get; internal set; }

        internal GuildMemberRemoveEventArgs(User u, Guild g)
        {
            user = u;
            guild = g;
        }
    }

    public delegate void HeartBeatReceivedEventHandler(object sender, HeartBeatReceivedEventArgs e);

    public class HeartBeatReceivedEventArgs : EventArgs
    {
        public dynamic data;

        internal HeartBeatReceivedEventArgs(dynamic d)
        {
            data = d;
        }
    }

    public delegate void WebSocketDisconnectEventHandler(object sender, WebSocketDisconnectEventArgs e);

    public class WebSocketDisconnectEventArgs : EventArgs
    {
        internal WebSocketDisconnectEventArgs() {}
    }

    public delegate void ResumeEventHandler(object sender, ResumeEventArgs e);

    public class ResumeEventArgs : EventArgs
    {
        internal ResumeEventArgs() { }
    }

    public delegate void PresenceUpdateEventHandler(object sender, PresenceUpdateEventArgs e);

    public class PresenceUpdateEventArgs : EventArgs
    {
        public GuildUser user { get; internal set; }
        public Guild guild { get; internal set; }

        internal PresenceUpdateEventArgs(GuildUser user, Guild guild) {
            this.user = user;
            this.guild = guild;
        }
    }

}
