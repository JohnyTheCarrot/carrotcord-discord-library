﻿using Carrotcord_API.Carrotcord.API;
using Carrotcord_API.Carrotcord.Stuff.Channels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Carrotcord_API.Carrotcord.Stuff
{
    public class Channel
    {
        public long ID;
        public long guild_id;
        public long last_message_id;
        public List<User> recipients = new List<User>(); //DM
        public string icon; //DM
        public long owner_id; //DM
        public int type;
        public ChannelType channelType;
        public string name;
        public string topic;
        public bool nsfw;
        public int position;
        public string data;
        public long application_id; //DM, if creator is bot

        public enum ChannelType
        {
            GUILD_TEXT = 0,
            DM = 1,
            GUILD_VOICE = 2,
            GROUP_DM = 3,
            GUILD_CATEGORY = 4
        }

        public static Channel createChannel(long guild_id, string name, ChannelType type)
        {
            if (String.IsNullOrEmpty(name)) throw new ArgumentException("Channel name can not be empty.");
            IRestResponse response = RestApiClient.POST($"https://discordapp.com/api/v6/guilds/{guild_id}/channels", new { name = name, type = type });
            return fromData(JSONDeserializeAndHandleErrors.DeserializeJSON(response));
        }

        public static Channel createChannel(string name, List<PermissionOverride> permissionOverrides, ChannelType type)
        {
            throw new NotImplementedException();
        }

        public void slowmode(int length)
        {
            RestApiClient.PATCH_v7("channels/" + ID, new { rate_limit_per_user = length });
        }

        public static Channel createChannel(string name, Channel parent, List<PermissionOverride> permissionOverrides, ChannelType type)
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            RestApiClient.DELET_THIS($"https://discordapp.com/api/v6/channels/{ID}");
        }

        public Guild getGuild()
        {
            if (getChannelType() == ChannelType.DM || getChannelType() == ChannelType.GROUP_DM) throw new InvalidOperationException("Cannot get guild of channel type " + getChannelType());
            else
            {
                return Guild.getGuild(guild_id);
            }
        }

        public static Channel getChannel(long ID)
        {
            if (Storage.cachedChannels.TryGetValue(ID, out Channel value))
            {
                return value;
            }
            IRestResponse response = RestApiClient.GET("channels/" + ID);
            //Console.WriteLine(response.Content);
            Channel channel = fromData(JSONDeserializeAndHandleErrors.DeserializeJSON(response));
            Storage.cachedChannels.TryAdd(channel.ID, channel);
            return channel;
        }

        public static Channel GetGuildChannels(long guild_id)
        {
            throw new NotImplementedException();
        }

        //[RequireChannelType(ChannelType.GUILD_TEXT)]
        public void sendMessage(string message)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POSTDiscordMessage($"channels/{ID}/messages", message));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

        public void sendMessage(string message, DiscordEmbed embed)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                embed.checkLimits();
                Console.WriteLine(JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POST($"channels/{ID}/messages", Message.getJSON(message, embed.toJSON()))));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

        /**public List<Message> getPins()
        {
            List<Message> pins = new List<Message>();
            dynamic data = JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.GET($"channels/{ID}/pins"));
            for(int i = 0; i < data.Length; i++)
            {
                pins.Add(Message.fromJSON(data[i]));
            }
            return pins;
        }*/

        public static bool isGuildChannel(Channel channel)
        {
            if (channel.getChannelType() == Channel.ChannelType.DM || channel.getChannelType() == Channel.ChannelType.GROUP_DM) return false;
            else return true;
        }

        public bool isGuildChannel()
        {
            if (getChannelType() == ChannelType.DM || getChannelType() == ChannelType.GROUP_DM) return false;
            else return true;
        }

        public string getChannelReference()
        {
            return $"<#{ID}>";
        }

        internal static Channel fromData(long guildID, dynamic data)
        {
            if (Storage.cachedChannels.TryGetValue(Convert.ToInt64(data.id), out Channel value))
            {
                return value;
            }
            
            ChannelType type = (ChannelType)Convert.ToInt32(data.type);
            if (type == ChannelType.GUILD_TEXT)
            {
                GuildTextChannel textChannel =  GuildTextChannel.TextChannelFromData(data);
                if (!Storage.cachedChannels.TryAdd(textChannel.ID, textChannel)) CarrotcordLogger.LogBork($"Channel.fromData() => Failed to cache channel {textChannel.name} ({textChannel.ID}).");
                //CarrotcordLogger.log(CarrotcordLogger.LogSource.BOT, "Channel.fromData(), guild text");
                return textChannel;
            }
            if (type == ChannelType.DM)
            {
                DMChannel dmChannel = DMChannel.DMChannelFromData(data);
                if(!Storage.cachedChannels.TryAdd(dmChannel.ID, dmChannel)) CarrotcordLogger.LogBork($"Channel.fromData() => Failed to cache channel {dmChannel.name} ({dmChannel.ID}).");
                //CarrotcordLogger.log(CarrotcordLogger.LogSource.BOT, "Channel.fromData(), DM");
                return dmChannel;
            }
            Channel channel = new Channel();
            //Console.WriteLine(JsonConvert.SerializeObject(data));
            channel.data = JsonConvert.SerializeObject(data, Formatting.Indented);
            channel.ID = Convert.ToInt64(data.id);
            channel.type = Convert.ToInt32(data.type);
            channel.name = data.name;
            if (data.position != null) channel.position = data.position;
            channel.topic = data.topic;
            channel.guild_id = guildID;
            //CarrotcordLogger.log(CarrotcordLogger.LogSource.BOT, $"Intializing channel type {(ChannelType)Convert.ToInt32(data.type)}, guild ID is null? {channel.guild_id == null}");
            if (data.recipients != null)
            {
                for (int i = 0; i < data.recipients.Count; i++)
                {
                    channel.recipients.Add(User.FromData(data.recipients[i]));
                }
            }
            Storage.cachedChannels.TryAdd(channel.ID, channel);
            return channel;
        }

        internal static protected Channel fromData(dynamic data)
        {
            return fromData(Convert.ToInt64(data.guild_id), data);
        }

        public ChannelType getChannelType()
        {
            return (ChannelType)type;
        }

        public static ChannelType getChannelType(int data)
        {
            return (ChannelType)data;
        }

        internal static ChannelType getChannelType(dynamic data)
        {
            return Convert.ToInt32(data.type);
        } 

    }
}
