﻿using Carrotcord_API.Carrotcord.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff.Channels
{
    public class DMChannel : Channel
    {
        internal static DMChannel DMChannelFromData(dynamic data)
        {
            DMChannel channel = new DMChannel();
            if(data.last_message_id!=null) channel.last_message_id = Convert.ToInt64(data.last_message_id);
            channel.ID = data.id;
            channel.type = data.type;
            if (data.recipients != null)
            {
                for (int i = 0; i < data.recipients.Count; i++)
                {
                    channel.recipients.Add(User.FromData(data.recipients[i]));
                }
            }
            return channel;
        }

        public void sendMessage(string message)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POSTDiscordMessage($"channels/{ID}/messages", message));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

        public void sendMessage(string message, DiscordEmbed embed)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                embed.checkLimits();
                JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POST($"channels/{ID}/messages", Message.getJSON(message, embed.toJSON())));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

    }
}
