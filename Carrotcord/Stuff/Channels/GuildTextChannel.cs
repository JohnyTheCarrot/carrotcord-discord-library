﻿using Carrotcord_API.Carrotcord.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff
{
    public class GuildTextChannel : Channel
    {
        internal static GuildTextChannel TextChannelFromData(dynamic data)
        {
            GuildTextChannel channel = new GuildTextChannel();
            if (data.guild_id != null) channel.guild_id = Convert.ToInt64(data.guild_id);
            channel.ID = data.id;
            channel.position = data.position;
            channel.data = JsonConvert.SerializeObject(data, Formatting.Indented);
            channel.name = data.name;
            channel.topic = data.topic;
            if (data.nsfw != null) channel.nsfw = data.nsfw;
            if (data.last_message_id != null) channel.last_message_id = Convert.ToInt64(data.last_message_id);
            channel.type = data.type;
            return channel;
        }

        public void sendMessage(string message)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POSTDiscordMessage($"channels/{ID}/messages", message));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

        public void sendMessage(string message, DiscordEmbed embed)
        {
            if (getChannelType() != ChannelType.GUILD_CATEGORY && getChannelType() != ChannelType.GUILD_VOICE)
            {
                embed.checkLimits();
                JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.POST($"channels/{ID}/messages", Message.getJSON(message, embed.toJSON())));
            }
            else throw new InvalidOperationException("Cannot send message to channel of type " + getChannelType());
        }

        public List<Message> getPins()
        {
            List<Message> pins = new List<Message>();
            dynamic data = JSONDeserializeAndHandleErrors.DeserializeJSON(RestApiClient.GET($"channels/{ID}/pins"));
            for (int i = 0; i < data.Length; i++)
            {
                pins.Add(Message.fromJSON(data[i]));
            }
            return pins;
        }

    }
}
