﻿using Carrotcord_API.Carrotcord.API;
using Carrotcord_API.Carrotcord.HTTP;
using Carrotcord_API.Carrotcord.Stuff.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff
{
    public class Command
    {
        public string name;
        public Action<Message, List<string>> result;
        public Action<CommandContext> ResultContext;
        public Action<Exception, Message> onError;
        public Action<string, CommandContext> onCommandCanceled;
        public List<string> args = new List<string>();
        /// <summary>
        /// Use when you need an exact amount of arguments
        /// Will render <see cref="minArgs"/> and <see cref="maxArgs"/> useless.
        /// </summary>
        public int? requiredArgsExact;
        /// <summary>
        /// Use when you need a minimum amount of arguments, but you allow more than the minimum.
        /// Can't be used along with <see cref="requiredArgsExact"/>
        /// </summary>
        public int? minArgs;
        /// <summary>
        /// Use to declare a maximum amount of arguments.
        /// Can't be used along with <see cref="requiredArgsExact"/>
        /// </summary>
        public int? maxArgs;
        public bool catchError = true;
        public bool isSubCommand = false;
        public List<Command> subCommands = new List<Command>();

        //help
        public string usage = "";
        public string description = "";
        public string example = "";

        public List<CommandRequirement> requirements;

        /// <summary>
        /// Use when you want to initialize the variables by object reference
        /// </summary>
        public Command(){}

        /**[Obsolete("Depracated. Will work but use Command(string name, Action<CommandContext> context) or Command(string name, Action<Message, List<string>> result, Action<Exception, Message> onError) instead")]
        public Command(string name, Action<Message, List<string>> result)
        {
            this.name = name;
            this.result = result;
        }

        public Command(string name, Action<Message, List<string>> result, Action<Exception, Message> onError)
        {
            this.name = name;
            this.result = result;
            this.onError = onError;
        }*/

        /// <summary>
        /// Creates a command
        /// </summary>
        /// <param name="name">name of the command</param>
        /// <param name="context"></param>
        public Command(string name, Action<CommandContext> context)
        {
            this.name = name;
            ResultContext = context;
        }

        public Command(string name, List<CommandRequirement> requirements, Action<CommandContext> context)
        {
            this.name = name;
            ResultContext = context;
            this.requirements = requirements;
        }

        public void AddSubCommand(Command cmd)
        {
            subCommands.Add(cmd);
        }

        public void SetHelpInfo(string usage, string description, string example)
        {
            this.usage = usage;
            this.description = description;
            this.example = example;
        }

        public void SetHelpInfo(string usage, string description)
        {
            this.usage = usage;
            this.description = description;
        }

        internal void Cancel(string reason, CommandContext context)
        {
            onCommandCanceled?.Invoke(reason, context);
        }

        internal bool IsValid()
        {
            if (name == null || (result == null && ResultContext==null)) return false;
            else return true;
        }

        internal void Execute(Message message)
        {
            List<string> args;
            args = InitArgs(message);
            CheckRequirements(message);
            InvokeCommand(message);
        }

        internal List<string> InitArgs(Message message)
        {
            try
            {
                var args = message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList();
                if (result != null) return null;
                CommandContext context = new CommandContext(Channel.getChannel(message.channelID), message.author, message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>(), this);
                if (requiredArgsExact.HasValue && args.Count != requiredArgsExact)
                {
                    Cancel($"Amount of arguments does not match the required amount of {requiredArgsExact}", context);
                    return null;
                }
                if (minArgs.HasValue && args.Count < minArgs)
                {
                    Cancel($"Too little arguments given. Given: {args.Count} Minimum required: {minArgs}", context);
                    return null;
                }
                if (maxArgs.HasValue && args.Count > maxArgs)
                {
                    Cancel($"Too many arguments given. Given: {args.Count} Maximum: {maxArgs}", context);
                    return null;
                }
                return args;
            }catch(ArgumentOutOfRangeException)
            {
                return new List<string>();
            }
        }

        internal void CheckRequirements(Message message)
        {
            if (requirements != null)
            {
                foreach (CommandRequirement requirement in requirements)
                {
                    if (!requirement.checkCondition(new CommandContext(Channel.getChannel(message.channelID), message.author, message, args, this)))
                    {
                        Cancel(requirement.reason, new CommandContext(Channel.getChannel(message.channelID), message.author, message, args, this));
                        return;
                    }
                }
            }
        }

        internal void InvokeCommand(Message message)
        {
            if (result != null)
            {
                try
                {
                    message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1);
                    if (catchError)
                    {
                        try
                        {
                            result.Invoke(message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>());
                        }
                        catch (Exception ex)
                        {
                            if (onError != null)
                            {
                                onError.Invoke(ex, message);
                            }
                            else
                            {
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = ex.Message
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }
                    else result.Invoke(message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>());
                }
                catch (ArgumentOutOfRangeException)
                {
                    if (catchError)
                    {
                        try
                        {
                            result.Invoke(message, new List<string>());
                        }
                        catch (Exception ex)
                        {
                            if (onError != null) onError.Invoke(ex, message);
                            else
                            {
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = $"**Type:**: {ex.StackTrace}\n**Message:** {ex.Message}"
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }
                    else result.Invoke(message, new List<string>());
                }
            }
        }

        internal void execute2(Message message)
        {

            List<string> args;
            try
            {
                args = message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList();
                if (result != null) return;
                CommandContext context = new CommandContext(Channel.getChannel(message.channelID), message.author, message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>(), this);
                if (requiredArgsExact.HasValue && args.Count != requiredArgsExact)
                {
                    Cancel($"Amount of arguments does not match the required amount of {requiredArgsExact}", context);
                    return;
                }
                if(minArgs.HasValue && args.Count<minArgs)
                {
                    Cancel($"Too little arguments given. Given: {args.Count} Minimum required: {minArgs}", context);
                    return;
                }
                if(maxArgs.HasValue && args.Count>maxArgs)
                {
                    Cancel($"Too many arguments given. Given: {args.Count} Maximum: {maxArgs}", context);
                    return;
                }
            }
            catch(ArgumentOutOfRangeException)
            {
                args = new List<string>();
            }

            if(requirements!=null)
            {
                foreach(CommandRequirement requirement in requirements)
                {
                    if (!requirement.checkCondition(new CommandContext(Channel.getChannel(message.channelID), message.author, message, args, this)))
                    {
                        Cancel(requirement.reason, new CommandContext(Channel.getChannel(message.channelID), message.author, message, args, this));
                        return;
                    }
                }
            }
            if (result != null)
            {
                try {
                    //Console.WriteLine(message.content.Substring(Bot.current.prefix.Length + name.Length + 1));
                    message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1);
                    if(catchError)
                    {
                        try
                        {
                            result.Invoke(message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>());
                        }catch(Exception ex)
                        {
                            if(onError!=null)
                            {
                                onError.Invoke(ex, message);
                            }else
                            {
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = ex.Message
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }else result.Invoke(message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>());
                } catch(ArgumentOutOfRangeException)
                {
                    if(catchError)
                    {
                        try
                        {
                            result.Invoke(message, new List<string>());
                        }catch(Exception ex)
                        {
                            if (onError!=null) onError.Invoke(ex, message);
                            else
                            {
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = $"**Type:**: {ex.StackTrace}\n**Message:** {ex.Message}"
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }else result.Invoke(message, new List<string>());
                }
            }else if(ResultContext!=null)
            {
                try
                {
                    message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1);
                    if(catchError)
                    {
                        try
                        {
                            var arguments = message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>();
                            foreach (Command cmd in subCommands)
                            {
                                CarrotcordLogger.log(CarrotcordLogger.LogSource.BOT, $"\"{cmd.name}\" | \"{arguments[0]}\" || {arguments[0]==cmd.name}");
                                if (arguments[0] == cmd.name)
                                {
                                    cmd.Execute(message);
                                    CarrotcordLogger.log(CarrotcordLogger.LogSource.BOT, "maymays");
                                    return;
                                }
                            }
                            ResultContext.Invoke(new CommandContext(Channel.getChannel(message.channelID), message.author, message, arguments, this));
                        }
                        catch (Exception ex)
                        {
                            if(ex is RatelimitTriggeredException)
                            {
                                return;
                            }
                            if (onError != null)
                            {
                                onError.Invoke(ex, message);
                            }
                            else
                            {
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = ex.Message
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }else ResultContext.Invoke(new CommandContext(Channel.getChannel(message.channelID), message.author, message, message.content.Substring(DiscordBot.current.prefix.Length + name.Length + 1).Split(' ').ToList<string>(), this));
                }
                catch(ArgumentOutOfRangeException)
                {
                    if(catchError)
                    {
                        try
                        {
                            ResultContext.Invoke(new CommandContext(Channel.getChannel(message.channelID), message.author, message, new List<string>(), this));
                        }
                        catch (Exception ex)
                        {
                            if (onError != null)
                            {
                                onError.Invoke(ex, message);
                            }
                            else
                            {
                                if(ex is RatelimitTriggeredException)
                                {
                                    return;
                                }
                                DiscordEmbed embed = new DiscordEmbed()
                                {
                                    title = ex.GetType().ToString(),
                                    description = ex.Message
                                };
                                message.reply("Something borked. <:monkaSCD:444404955265236992>", embed);
                            }
                        }
                    }else ResultContext.Invoke(new CommandContext(Channel.getChannel(message.channelID), message.author, message, new List<string>(), this));
                }
            }
        }
    }
}
