﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff
{
    public class CommandContext
    {

        public Channel channel;
        public Guild guild;
        public User author;
        public GuildUser member;
        public Message message;
        public List<String> args { get; internal set; }
        public bool fromGuild = true;
        internal Command command;

        internal CommandContext(Channel channel, User author, Message message, List<String> args, Command command)
        {
            this.channel = channel;
            try
            {
                guild = channel.getGuild();
            }
            catch (InvalidOperationException) { fromGuild = false; }
            this.author = author;
            if (fromGuild) member = guild.getMember(member);
            this.message = message;
            this.args = args;
            this.command = command;
        }

    }
}
