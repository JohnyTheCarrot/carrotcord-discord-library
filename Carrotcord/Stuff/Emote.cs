﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff
{
    public class Emote
    {
        public string name;
        public long ID;
        public bool animated;

        public Emote(string name, long ID)
        {
            this.name = name;
            this.ID = ID;
            this.animated = false;
        }

        public Emote(string name, long ID, bool animated)
        {
            this.name = name;
            this.ID = ID;
            this.animated = animated;
        }

        public static Emote Parse(string text)
        {
            try
            {
                text = text.Replace("<", "").Replace(">", "");
                string[] args = text.Split(':');
                bool anim = (String.IsNullOrEmpty(args[0]) == true ? false : true);
                string name = args[1];
                long ID = Convert.ToInt64(args[2]);
                return new Emote(name, ID, anim);
            }catch(Exception)
            {
                throw new ArgumentException("Invalid Arguments");
            }
        }

        public override string ToString()
        {
            return $"{(animated==true?"a":"")}:{name}:{ID}";
        }

    }
}
