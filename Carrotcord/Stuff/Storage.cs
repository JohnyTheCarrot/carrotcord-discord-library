﻿using Carrotcord_API.Carrotcord.API;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.Stuff
{
    class Storage
    {
        public static Dictionary<long, Message> cachedMessages = new Dictionary<long, Message>();
        public static Dictionary<long, Guild> cachedGuilds = new Dictionary<long, Guild>();
        public static ConcurrentDictionary<long, Channel> cachedChannels = new ConcurrentDictionary<long, Channel>();
        public static ConcurrentDictionary<long, User> cachedUsers = new ConcurrentDictionary<long, User>();
        public static Dictionary<string, Invite> cachedInvites = new Dictionary<string, Invite>();
    }
}
