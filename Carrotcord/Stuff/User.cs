﻿using Carrotcord_API.Carrotcord.API;
using Carrotcord_API.Carrotcord.Stuff;
using Carrotcord_API.Carrotcord.Stuff.Channels;
using RestSharp;
using System;

namespace Carrotcord_API.Carrotcord
{
    public class User
    {

        public string username;
        public string avatar;
        public string discriminator;
        /// <summary>
        /// turns for example 1 into 0001
        /// </summary>
        public string discriminatorString;
        public long ID;
        public bool bot;

        public string getAvatarURL()
        {
            return "https://cdn.discordapp.com/avatars/" + ID + "/" + avatar;
        }

        /// <summary>
        /// Returns the discriminator as shown in Discord. For example: 1 -> 0001
        /// </summary>
        /// <param name="discrim">discriminator</param>
        /// <returns></returns>
        [Obsolete("deprecated", true)]
        public static string FixDiscrim(int discrim)
        {
            return null;
            if (discrim < 10) return "000" + discrim;
            else if (discrim < 100) return "00" + discrim;
            else if (discrim < 1000) return "0" + discrim;
            return $"{discrim}";
        }

        public string ping()
        {
            return $"<@{ID}>";
        }

        public DMChannel CreateDMChannel()
        {
            Console.WriteLine("create");
            IRestResponse response = RestApiClient.POST("users/@me/channels", new { recipient_id=ID });
            return (DMChannel) Channel.fromData(JSONDeserializeAndHandleErrors.DeserializeJSON(response));
        }

        public static void FindUser(long ID)
        {
            Console.WriteLine(RestApiClient.PATCH("users/" + ID).Content);
        }

        internal static User FromData(dynamic data)
        {
            User user = new User();
            dynamic authorData = data;
            user.username = Convert.ToString(authorData.username);
            user.ID = Convert.ToInt64(authorData.id);
            user.discriminator = Convert.ToString(authorData.discriminator);
            user.avatar = Convert.ToString(authorData.avatar);
            user.bot = Convert.ToBoolean(authorData.bot);
            Storage.cachedUsers.AddOrUpdate(user.ID, user, (key, _user) => _user);
            return user;
        }

        internal static string ToJSON(User user)
        {
            //"author":{ "username":"JohnyTheCarrot","id":"132819036282159104","discriminator":"0001","avatar":"a_6fa93287f72ae7ef6606860a97091655"}
            return $"{{ \"username\": {user.username}, \"id\": \"{user.ID}\", \"discriminator\":\"{user.discriminator}\", \"avatar\": \"{user.avatar}\" }}";
        }

        public static void GetGuildMembers(long guildID)
        {
            throw new NotImplementedException();
            //Console.WriteLine($"{{\"op\": 8, \"d\": {{\"guild_id\": \"{guildID}\", \"querry\": \"\", \"limit\": 50}}}}");
            //Bot.send($"{{\"op\":8,\"d\":{{\"guild_id\":\"{guildID}\",\"querry\":\"\",\"limit\":50}}}}");
            //Bot.send("{\"op\":8,\"d\":{\"guild_id\":\"392800140974358528\",\"querry\":\"\",\"limit\":3}}");
        }

    }
}
