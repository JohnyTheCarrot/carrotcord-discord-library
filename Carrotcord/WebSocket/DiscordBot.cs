﻿using Carrotcord_API.Carrotcord;
using Carrotcord_API.Carrotcord.API;
using Carrotcord_API.Carrotcord.Events;
using Carrotcord_API.Carrotcord.Stuff;
using Carrotcord_API.Carrotcord.Users;
using Carrotcord_API.Carrotcord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using WebSocketSharp;

namespace Carrotcord_API
{
    public class DiscordBot
    {
        /**private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
        static ConsoleEventDelegate handler;*/

        //WebSocket
        public WebSocket socket;
        private string gateway_url;
        public int gateway_version = 6;
        private string encoding = "json";
        private int heartbeat_interval;
        private Timer timer;

        //Discord
        public SelfUser self_user;
        public static DiscordBot current;
        public string token;
        public Dictionary<string, Command> commands = new Dictionary<string, Command>();
        public string prefix = "!";

        //Events
        public event PresenceUpdateEventHandler PresenceUpdateEvent;
        public event MessageCreatedEventHandler MessageCreatedEvent;
        public event ChannelCreatedEventHandler ChannelCreatedEvent;

        /// <summary>
        /// How many events the bot has seen.
        /// </summary>
        public int sequence;
        private string session_id;

        public DiscordBot() { }

        public DiscordBot(string token)
        {
            this.token = token;
            current = this;
        }

        public bool Login()
        {
            if (socket != null && socket.IsAlive) return false;
            LogWebSocket("[DISCOVERY] FETCHING GATEWAY URL...");
            dynamic DiscoveryData = JsonConvert.DeserializeObject(RestApiClient.GETNOAUTH("gateway").Content);
            if (DiscoveryData == null)
            {
                LogBork("[DISCOVERY FAILED] Failed to get Gateway URL. This might be due to an outage our a lack of internet connection.");
                return false;
            }
            LogWebSocket("[DISCOVERY] Success.");
            gateway_url = Convert.ToString(DiscoveryData.url);
            InitSocket();
            //handler = new ConsoleEventDelegate(ConsoleEventCallback);
            //SetConsoleCtrlHandler(handler, true);
            return true;
        }

        public void Disconnect()
        {
            LogWebSocket("Closing socket");
            socket.CloseAsync(CloseStatusCode.Normal);
        }

        public void Resume()
        {
            InitSocket();
            socket.ConnectAsync();
            var data = new
            {
                token = token,
                session_id = session_id,
                seq = sequence
            };
            socket.Send(JsonConvert.SerializeObject(data));
        }

        static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                Console.WriteLine("Console window closing, death imminent");
                DiscordBot.current.socket.CloseAsync(CloseStatusCode.Normal);
            }
            return false;
        }

        private void InitSocket()
        {
            LogWebSocket("Initializing Websocket");
            socket = new WebSocket($"{gateway_url}/?v={gateway_version}&encoding={encoding}");
            socket.OnMessage += Socket_OnMessage;
            socket.OnClose += Socket_OnClose;
            socket.ConnectAsync();
        }

        private void Socket_OnClose(object sender, CloseEventArgs e)
        {
            LogWebSocket($"[{e.Code}] {e.Reason}");
        }

        private void Socket_OnMessage(object sender, MessageEventArgs e)
        {
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine(e.Data);
            DataHandler(new GatewayPayload((dynamic)JsonConvert.DeserializeObject(e.Data)));
        }

        private void DataHandler(GatewayPayload payload)
        {
            OPCode opcode = payload.Opcode;
            //LogWebSocketIn($"[{(int)opcode}] {opcode} " + (opcode==OPCode.DISPATCH?payload.EventName:""));
            switch (opcode)
            {
                case OPCode.DISPATCH:
                    DISPATCH(payload);
                    break;
                case OPCode.HELLO:
                    HELLO(payload.Data);
                    break;
            }
        }

        internal void DISPATCH(GatewayPayload payload)
        {
            GatewayEvent event_name = (GatewayEvent) Enum.Parse(typeof(GatewayEvent), payload.EventName);
            sequence = payload.Sequence;
            Console.SetCursorPosition(0, Console.CursorTop);
            switch (event_name)
            {
                case GatewayEvent.READY:
                    READY(payload.Data);
                    break;
                case GatewayEvent.GUILD_CREATE:
                    GUILD_CREATE(payload.Data);
                    break;
                case GatewayEvent.PRESENCE_UPDATE:
                    PRESENCE_UPDATE(payload.Data);
                    break;
                case GatewayEvent.MESSAGE_CREATE:
                    MESSAGE_CREATE(payload.Data);
                    break;
                case GatewayEvent.CHANNEL_CREATE:
                    CHANNEL_CREATE(payload.Data);
                    break;
                case GatewayEvent.CHANNEL_PINS_UPDATE:

                    break;
            }
        }

        //Dispatch events
        internal void READY(dynamic data)
        {
            session_id = Convert.ToString(data.session_id);
            LogWebSocket("|-------------------------------------------------------|");
            LogWebSocket("|   _____                     _                    _    |");
            LogWebSocket("|  / ____|                   | |                  | |   |");
            LogWebSocket("| | |     __ _ _ __ _ __ ___ | |_ ___ ___  _ __ __| |   |");
            LogWebSocket("| | |    / _` | '__| '__/ _ \\| __/ __/ _ \\| '__/ _` |   |");
            LogWebSocket("| | |___| (_| | |  | | | (_) | || (_| (_) | | | (_| |   |");
            LogWebSocket("|  \\_____\\__,_|_|  |_|  \\___/ \\__\\___\\___/|_|  \\__,_|   |");
            LogWebSocket("|  _      _ _                                           |");
            LogWebSocket("| | |    (_| |                                          |");
            LogWebSocket("| | |     _| |__  _ __ __ _ _ __ _   _                  |");
            LogWebSocket("| | |    | | '_ \\| '__/ _` | '__| | | |                 |");
            LogWebSocket("| | |____| | |_) | | | (_| | |  | |_| |                 |");
            LogWebSocket("| |______|_|_.__/|_|  \\__,_|_|   \\__, |                 |");
            LogWebSocket("|                                 __/ |                 |");
            LogWebSocket("|                                |___/                  |");
            LogWebSocket("|                                                       |");
            LogWebSocket("|-------------------------------------------------------|");
            LogWebSocket("|        Discord library made by JohnyTheCarrot         |");
            LogWebSocket("|-------------------------------------------------------|");
            /**LogWebSocket("   _____                            _           _ ");
            LogWebSocket("  / ____|                          | |         | |");
            LogWebSocket(" | |     ___  _ __  _ __   ___  ___| |_ ___  __| |");
            LogWebSocket(" | |    / _ \\| '_ \\| '_ \\ / _ \\/ __| __/ _ \\/ _` |");
            LogWebSocket(" | |___| (_) | | | | | | |  __| (__| ||  __| (_| |");
            LogWebSocket("  \\_____\\___/|_| |_|_| |_|\\___|\\___|\\__\\___|\\__,_|");*/
        }

        internal void GUILD_CREATE(dynamic data)
        {
            //LogWebSocket(JsonConvert.SerializeObject(data));
            Guild.fromJSON(data);
        }

        internal void PRESENCE_UPDATE(dynamic data)
        {
            Guild g = Guild.getGuild(Convert.ToInt64(data.guild_id));
            GuildUser user = g.getMember(Convert.ToInt64(data.user.id));
            //Role
            PresenceUpdateEvent?.Invoke(this, new PresenceUpdateEventArgs(user, g));
        }

        internal void MESSAGE_CREATE(dynamic data)
        {
            Console.WriteLine(JsonConvert.SerializeObject(data));
            User author = User.FromData(data.author);

            Message message = new Message();
            dynamic messageData = data;
            message.author = author;
            message.content = Convert.ToString(messageData.content);
            message.ID = Convert.ToInt64(messageData.id);
            message.channelID = Convert.ToInt64(messageData.channel_id);
            message.guildID = Convert.ToInt64(messageData.guild_id);
            message.Guild = Guild.getGuild(message.guildID);
            message.pinned = Convert.ToBoolean(messageData.pinned);
            if(Convert.ToInt32(data.mentions.Count)>0)
            {
                for(int i = 0; i < Convert.ToInt32(data.mentions.Count); i++)
                {
                    User user = User.FromData(data.mentions[i]);
                    message.mentions.Add(user);
                    Console.WriteLine(user.username);
                }
            }

            Storage.cachedMessages.Add(message.ID, message);
            MessageCreatedEvent?.Invoke(this, new MessageCreatedEventArgs(message));
            if (message.author.bot) return;
            foreach (Command cmd in commands.Values)
            {
                if (message.content.StartsWith(prefix) && message.content.Substring(1).Split(' ')[0] == cmd.name)
                {
                    cmd.execute2(message);
                    return;
                }
            }
        }

        internal void CHANNEL_CREATE(dynamic data)
        {
            //LogWebSocket(data);
            Channel c = Channel.fromData(data);
            ChannelCreatedEvent?.Invoke(this, new ChannelCreatedEventArgs(c));
        }

        //OPCodes

        internal void HELLO(dynamic data)
        {
            heartbeat_interval = Convert.ToInt32(data.heartbeat_interval);
            Console.WriteLine(heartbeat_interval);
            timer = new Timer(heartbeat_interval);
            timer.AutoReset = true;
            timer.Elapsed += HeartbeatTimer;
            timer.Start();

            SendMessage(new { op = 1, d = (string)null });

            var identify_payload = new
            {
                op = 2,
                d = new
                {
                    token = "" + token,
                    properties = new
                    {
                        os = "windows",
                        browser = "Carrotcord Discord Library",
                        device = "Carrotcord Discord Library"
                    },
                    large_threshold = 250,
                    presence = new
                    {
                        status = "online",
                        afk = false/**,
                        activities = new object[]
                        {
                            new
                            {
                                name = "sajuuk shitpost",
                                type = 3
                            }
                        }*/
                    }
                }
            };
            SendMessage(identify_payload);
        }

        private void HeartbeatTimer(object sender, ElapsedEventArgs e)
        {
            if (socket.IsAlive)
            {
                SendHeartbeat();
            }
        }

        private void SendHeartbeat()
        {
            SendMessage(new { op = 1, d = (string)null });
        }

        public void SendMessage(dynamic msg)
        {
            var payload = JsonConvert.SerializeObject(msg);
            //if(socket.IsAlive)
            //{
                socket.Send(payload);
                LogWebSocketOut($"[{msg.op}]");
            //}
        }

        internal void LogWebSocket(string msg) => CarrotcordLogger.LogClient(CarrotcordLogger.LogSource.WEBSOCKET, msg);
        internal void LogWebSocketIn(string msg) => LogWebSocket("<= " + msg);
        internal void LogWebSocketOut(string msg) => LogWebSocket("=> " + msg);
        internal void LogBork(string msg) => CarrotcordLogger.LogBork(msg);

        //Commands

        public DiscordBot RegisterCommand(Command command)
        {
            if (!command.IsValid()) throw new NullReferenceException("Either the name or result variable has not been initialized!");
            foreach (string cmd in commands.Keys)
            {
                if (cmd == command.name)
                {
                    CarrotcordLogger.LogBork($"There already is a command registered by the name of \"{command.name}\", skipping.");
                    return this;
                }
            }
            CarrotcordLogger.LogBork("Command registered");
            commands.Add(command.name, command);
            return this;
        }

        public Command getCommand(string name)
        {
            Command command;
            if (commands.TryGetValue(name, out command))
            {
                return command;
            }
            else return null;
        }
    }
}
