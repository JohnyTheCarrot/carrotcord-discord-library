﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.WebSocket
{
    internal enum GatewayEvent
    {
        HELLO,
        READY,
        RESUMED,
        GUILD_CREATE,
        GUILD_BAN_ADD,
        GUILD_MEMBER_ADD,
        GUILD_MEMBER_UPDATE,
        GUILD_MEMBER_REMOVE,
        PRESENCE_UPDATE,
        PRESENCES_REPLACE,
        MESSAGE_CREATE,
        MESSAGE_UPDATE,
        TYPING_START,
        MESSAGE_REACTION_ADD,
        MESSAGE_REACTION_REMOVE,
        MESSAGE_DELETE,
        CHANNEL_CREATE,
        CHANNEL_PINS_UPDATE,
        VOICE_STATE_UPDATE
    }
}
