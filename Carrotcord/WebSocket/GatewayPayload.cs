﻿using Carrotcord_API.Carrotcord.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.WebSocket
{
    internal class GatewayPayload
    {
        private OPCode opcode;
        private dynamic all_data;
        private int sequence;
        private string event_name = null;
        private dynamic data;

        internal GatewayPayload(dynamic data)
        {
            opcode = (OPCode)Enum.ToObject(typeof(OPCode), Convert.ToInt16(data.op));
            this.data = data.d;
            all_data = data;
            if(opcode==OPCode.DISPATCH)
            {
                sequence = Convert.ToInt32(data.s);
                event_name = Convert.ToString(data.t);
            }
        }

        public string EventName { get => event_name; private set => event_name = value; }
        public dynamic Data { get => data; private set => data = value; }
        public dynamic AllData { get => all_data; private set => all_data = value; }
        public OPCode Opcode { get => opcode; private set => opcode = value; }
        public int Sequence { get => sequence; private set => sequence = value; }
    }
}
