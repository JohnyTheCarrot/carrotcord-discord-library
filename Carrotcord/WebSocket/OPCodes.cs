﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrotcord_API.Carrotcord.API
{
    public enum OPCode
    {
        DISPATCH = 0,
        HEARTBEAT = 1,
        IDENTIFY = 2,
        STATUS_UPDATE = 3,
        VOICE_STATE_UPDATE = 4,
        VOICE_SERVER_PING = 5,
        RESUME = 6,
        RECONNECTED = 7,
        REQUEST_GUILD_MEMBERS = 8,
        INVALID_SESSION = 9,
        HELLO = 10,
        HEARTBEAT_ACK = 11
    }
}
